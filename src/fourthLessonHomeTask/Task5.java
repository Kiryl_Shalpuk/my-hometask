package fourthLessonHomeTask;

public class Task5 {
    // Возвести 2 числа в любую степень и вывести их сумму
    public static void main(String[] args) {
        double r = cub(4, 3);
        System.out.println("Сумма чисел " + r);
    }

    public static double cub(int numb1, int numb2){
        double step1 = (int) (Math.random()*7);
        double step2 = (int) (Math.random()*7);
        double Cnumb1 = Math.pow(numb1, step1);
        double Cnumb2 = Math.pow(numb2, step2);
        System.out.println(numb1 + " в степени  " + step1 + " и " + numb2 + " в степени  " + step2);
        return Cnumb1 + Cnumb2;
    }
}
