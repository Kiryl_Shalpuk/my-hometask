package fourthLessonHomeTask;

public class Task3 {
    // Вывести квадрат числа и квадрат числа + 2
    public static void main(String[] args) {
        int array[] = new int[10];
        for (int i = 1; i < array.length; i++){
            array[i] = i;
            int r = square(array[i]);
            System.out.print("\n" + r + " " + (r+2));
        }
    }

    public static int square(int numb){
        return numb * numb;
    }
}
