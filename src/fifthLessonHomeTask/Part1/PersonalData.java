package fifthLessonHomeTask.Part1;

public class PersonalData {
    public static String person[][] = new String[9][4];

    // получить информацию о человеке
    protected static void setPerson(){
        for (int i = 0; i < person.length; i++){
            for (int j = 0; j < person[i].length; j++){
                person[i][j] = createPerson(i, j);
                showPerson(j, person[i][j]);
            }
        }
    }

    // создать человека
    private static String createPerson(int n, int m){
        String name[] = {"Брэд Питт", "Анджелина Джоли", "Уильям Питт", "Джейн Хиллхаус", "Джон Войт",
                "Маршелин Бертран", "Шайло Джоли-Питт", "Нокс Джоли-Питт", "Вивьен Джоли-Питт"};
        String age[] = {"54", "43", "82", "76", "69", "65", "12", "10", "10"};
        String sex[] = {"Мужской", "Женский", "Мужской", "Женский", "Мужской", "Женский",
                "Женский", "Мужской", "Женский"};
        String familyStatus[] = {"Сын Уильяма Питта и Джейн Хиллхаус; Отец Шайло Джоли-Питт, Нокса Джоли-Питта " +
                "и Вивьен Джоли-Питт",
                "Дочь Джона Войта и Маршелины Бертран; Мать Шайло Джоли-Питт, Нокса Джоли-Питта и Вивьен Джоли-Питт",
                "Отец Брэда Питта; Дед Шайло Джоли-Питт, Нокса Джоли-Питта и Вивьен Джоли-Питт",
                "Мать Брэда Питта; Бабушка Шайло Джоли-Питт, Нокса Джоли-Питта и Вивьен Джоли-Питт",
                "Отец Анджелины Джоли; Дед Шайло Джоли-Питт, Нокса Джоли-Питта и Вивьен Джоли-Питт",
                "Мать Анджелины Джоли; Бабушка Шайло Джоли-Питта, Нокса Джоли-Питта и Вивьен Джоли-Питт",
                "Дочь Брэда Питта и Анджелины Джоли; Сестра Нокса Джоли-Питта и Вивьен Джоли-Питт",
                "Сын Брэда Питта и Анджелины Джоли; Брат Шайло Джоли-Питт и Вивьен Джоли-Питт",
                "Дочь Брэда Питта и Анджелины Джоли; Сестра Шайло Джоли-Питт и Нокса Джоли-Питта"};
        for (int i = 0; i < person.length; i++) {
            person[i][0] = name[i];
            person[i][1] = age[i];
            person[i][2] = sex[i];
            person[i][3] = familyStatus[i];
        }
        return person[n][m];
    }

    // показать информацию о человеке
    private static void showPerson(int j, String person){
        if (j == 0){
            System.out.println("~ ~ ~\t~ ~ ~\t~ ~ ~\t~ ~ ~\t~ ~ ~\t~ ~ ~\t~ ~ ~");
            System.out.println("Имя: " + person + ".");
        }
        if (j == 1){
            System.out.print("Возраст: " + person);
            if (Integer.valueOf(person) % 10 == 1 || Integer.valueOf(person) == 1){
                System.out.println(" год.");
            }
            else if (Integer.valueOf(person) > 9 && Integer.valueOf(person) < 20){
                System.out.println(" лет.");
            }
            else if (Integer.valueOf(person) % 10 > 1 && Integer.valueOf(person) % 10 < 5){
                System.out.println(" года.");
            }
            else if (Integer.valueOf(person) % 10 >= 5 && Integer.valueOf(person) > 4){
                System.out.println(" лет.");
            }
            else {
                System.out.println(" лет.");
            }
        }
        if (j == 2){
            System.out.println("Пол: " + person + ".");
        }
        if (j == 3){
            System.out.println("Роль в семье: " + person + ".");
        }
    }
}
