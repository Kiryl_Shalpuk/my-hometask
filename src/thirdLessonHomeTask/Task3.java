package thirdLessonHomeTask;

public class Task3 {
    public static void main (String[] args){
        // Задача 3. Массив из 15 рандомных чисел от 0 до 9. Найти количество четных чисел
        System.out.println("Задача №3 ");
        int even = 0;
        int massiv3[] = new int [15];
        for (int i = 0; i < massiv3.length; i++){
            massiv3[i] = 0 + (int)(Math.random()*9);
            System.out.print(massiv3[i] + " ");
            if (massiv3[i] % 2 == 0 && massiv3[i] != 0){
                even++;
            }
        }
        System.out.println("\nКолличество четных чисел: " + even);
    }
}
