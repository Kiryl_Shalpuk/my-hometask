package thirdLessonHomeTask;

public class Task5 {
    public static void main(String[] args) {
        // Задача 5. Два рандомных массива от 0 до 5. Вывести на экран оба,
        //посчитать среднее арифметическое и сравнить
        System.out.println("Задача №5 ");
        int massiv5_1[] = new int[5];
        int massiv5_2[] = new int[5];
        int summ_massiv5_1 = 0, summ_massiv5_2 = 0;
        int medium_summ_massiv5_1 = 0, medium_summ_massiv5_2 = 0;
        for (int i = 0; i < massiv5_1.length; i++) {
            massiv5_1[i] = (int) (Math.random() * 5);
            summ_massiv5_1 = summ_massiv5_1 + massiv5_1[i];
            System.out.print(massiv5_1[i] + " ");

        }
        medium_summ_massiv5_1 = summ_massiv5_1 / massiv5_1.length;
        System.out.println(" ");
        for (int i = 0; i < massiv5_2.length; i++) {
            massiv5_2[i] = (int) (Math.random() * 5);
            summ_massiv5_2 = summ_massiv5_2 + massiv5_2[i];
            System.out.print(massiv5_2[i] + " ");
        }
        medium_summ_massiv5_2 = summ_massiv5_2 / massiv5_2.length;

        System.out.println(" ");
        if (medium_summ_massiv5_1 > medium_summ_massiv5_2) {
            System.out.print("Среднее арифмитическое первого массива: " + medium_summ_massiv5_1 +
                    " больше среднего арифмитического второго: " + medium_summ_massiv5_2);
        } else if (medium_summ_massiv5_2 > medium_summ_massiv5_1) {
            System.out.print("Среднее арифмитическое первого массива: " + medium_summ_massiv5_1 +
                    " меньше среднего арифмитического второго: " + medium_summ_massiv5_2);
        } else {
            System.out.print("Среднее арифмитическое первого массива: " + medium_summ_massiv5_1 +
                    " равно среднему арифмитическому второго: " + medium_summ_massiv5_2);
        }
    }
}
