package thirdLessonHomeTask;

public class Task2 {
    public static void main (String[] args){
        // Задача 2. Массив из нечетных чисел от 1 до 99. Потом в обратном порядке
        System.out.println("Задача №2 ");
        int massiv2[] = new int [100];
        for (int i = 1; i < massiv2.length; i++ ){
            massiv2[i] = i;
            if (i % 2 != 0){
                System.out.print(massiv2[i] + " ");
            }
        }
        System.out.println(" ");
        for (int i = massiv2.length; i > 0; i--){
            if (i % 2 != 0){
                System.out.print(massiv2[i] + " ");
            }
        }
    }
}
