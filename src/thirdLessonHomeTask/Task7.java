package thirdLessonHomeTask;

public class Task7 {
    public static void main (String[] args){
        // Задача 7. То же самое для самого короткого массива.
        System.out.println("Задача №7 ");
        int massiv7[][] = new int [5][];
        int short_massiv = 0;
        int summ_short_massiv = 0;
        int number_short_massiv = 0;
        for (int i = 0; i < massiv7.length; i++){
            massiv7[i] = new int [i + 2];
            for (int j = 0; j < massiv7[i].length; j++){
                massiv7[i][j] = 1 + (int)(Math.random()*10);
                if (j % massiv7.length == massiv7[i].length - 1){
                    System.out.println(massiv7[i][j] + "\t");
                }
                else {
                    System.out.print(massiv7[i][j] + "\t");
                }
                if (short_massiv > massiv7[i].length){
                    number_short_massiv = i;
                    short_massiv = massiv7[i].length;
                }
            }
        }
        for (int i = 0; i < massiv7[number_short_massiv].length; i ++){
            summ_short_massiv = summ_short_massiv + massiv7[number_short_massiv][i];
        }
        System.out.println("\nСумма короткого массива: " + summ_short_massiv);
    }
}
