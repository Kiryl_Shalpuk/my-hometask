package ninethLessonHomeTask.Part2;

import ninethLessonHomeTask.Part2.HoursThread;

public class Main {

    public static void main(String[] args) {
        HoursThread hoursThread = new HoursThread();
        hoursThread.start();
        try {
            hoursThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
