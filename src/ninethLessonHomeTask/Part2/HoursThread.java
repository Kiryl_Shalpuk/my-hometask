package ninethLessonHomeTask.Part2;

import java.time.LocalDateTime;

public class HoursThread extends Thread {

    HoursThread(){
    }

    @Override
    public void run(){
        LocalDateTime timeNow = LocalDateTime.now();
        System.out.println("Сейчас: " + timeNow.getHour() + ":" + timeNow.getMinute());
        int timeDifference;
        if (timeNow.getMinute() == 0 | timeNow.getMinute() == 30){
            System.out.println("Настало Ваше время...");
        }
        else{
            if (timeNow.getMinute() < 30){
                System.out.println("Следующая отправка в " + timeNow.getHour() + ":30");
                timeDifference = 29 - timeNow.getMinute();
            }
            else {
                System.out.println("Следующая отправка в " + (timeNow.getHour() + 1) + ":00");
                timeDifference = 59 - timeNow.getMinute();
            }
            System.out.println("Поток в ожидании. Осталось: " + timeDifference + " минут, " + (60 - timeNow.getSecond()) + " секунд");
            sleepThread(timeDifference * 60000 + timeNow.getSecond() * 1000);
            run();
        }
    }

    public void sleepThread(int milliseconds){
        try {
            sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
