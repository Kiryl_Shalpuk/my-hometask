package eigthLessonHomeTask.Part2;

import java.util.ArrayDeque;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class Market {
    ArrayDeque<Thread> queue1 = new ArrayDeque<>();
    ArrayDeque<Thread> queue2 = new ArrayDeque<>();
    ArrayDeque<Thread> queue3 = new ArrayDeque<>();

    Semaphore semaphoreQueue1 = new Semaphore(1);
    Semaphore semaphoreQueue2 = new Semaphore(1);
    Semaphore semaphoreQueue3 = new Semaphore(1);

    int longSize;
    int f = 1;

    int serviseCashTime1 = 0;
    int serviseCashTime2 = 0;
    int serviseCashTime3 = 0;

    public void queue(){

        addThreadList();
        System.out.println(queue1.size() + " " + queue2.size() + " " + queue3.size());

        for (int i = 0; i < longSize; i++){
            queue1.pollFirst().start();
            queue2.pollFirst().start();
            queue3.pollFirst().start();
        }
    }

    public void addThreadList(){
        if (f <= 27){
            if (queue1.size() <= queue2.size() & queue1.size() <= queue3.size()){
                queue1.add(new Customer(String.valueOf(f),
                        new Random().nextInt(5000) + 1000, 1, semaphoreQueue1));
                longSize = queue1.size();
            }
            else if (queue2.size() <= queue1.size() & queue2.size() <= queue3.size()){
                queue2.add(new Customer(String.valueOf(f),
                        new Random().nextInt(5000) + 1000, 2, semaphoreQueue2));
                longSize = queue2.size();
            }
            else if (queue3.size() <= queue1.size() & queue3.size() <= queue2.size()){
                queue3.add(new Customer(String.valueOf(f),
                        new Random().nextInt(5000) + 1000, 3, semaphoreQueue3));
                longSize = queue3.size();
            }
            f++;
            addThreadList();
        }
    }
}
