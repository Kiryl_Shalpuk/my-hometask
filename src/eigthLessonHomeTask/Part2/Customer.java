package eigthLessonHomeTask.Part2;

import java.util.concurrent.Semaphore;

public class Customer extends Thread {
    int timeService;
    String nameCustomer;
    int cashName;
    Semaphore semaphore;

    String blueText = "\u001B[36m", yellowText = "\u001B[32m", whiteText = "\u001B[30m", resetText = "\u001B[0m";

    Customer(String nameCustomer, int timeService, int cashName, Semaphore semaphore){
        this.timeService = timeService;
        this.nameCustomer = nameCustomer;
        this.semaphore = semaphore;
        this.cashName = cashName;
    }

    @Override
    public void run() {
        try {
            semaphore.acquire();
            if (cashName == 1){
                System.out.println("Касса\t№" + cashName + "\tначала обслуживать клиента\t\t№" + nameCustomer);
            }
            else if (cashName == 2){
                System.out.println(blueText + "Касса\t№" + cashName + "\tначала обслуживать клиента\t\t№" + nameCustomer + resetText);
            }
            else {
                System.out.println(yellowText + "Касса\t№" + cashName + "\tначала обслуживать клиента\t\t№" + nameCustomer + resetText);
            }
            sleep(timeService);
            if (cashName == 1){
                System.out.println("Касса\t№" + cashName + "\tзакончила обслуживать клиента\t№" + nameCustomer + "\t\tВремя обслуживания " + timeService/1000 + "c");
            }
            else if (cashName == 2){
                System.out.println(blueText + "Касса\t№" + cashName + "\tзакончила обслуживать клиента\t№" + nameCustomer + "\t\tВремя обслуживания " + timeService/1000 + "c" + resetText);
            }
            else {
                System.out.println(yellowText + "Касса\t№" + cashName + "\tзакончила обслуживать клиента\t№" + nameCustomer + "\t\tВремя обслуживания " + timeService/1000 + "c" + resetText);
            }
            semaphore.release();
        } catch (InterruptedException e) {
            System.out.println("Вероятно очередь закончилась!");
        }

    }

}
