package eigthLessonHomeTask.Part1;

public class CountClass extends Thread{
    private StringBuffer text;
    private int countTo;
    private int sum = 0;
    public CountClass (StringBuffer s, int c) {
        text = s;
        countTo = c;
    }

    @Override
    public void run() {
        System.out.println("thread started");
        for (int i = 1; i <= countTo; i++){
            sum = sum + i;
        }
        text.append("nsum = " + sum + "\n");
        System.out.println("thread finished");
    }
}