package eigthLessonHomeTask.Part1;

public class Main {
    public static void main(String[] args) {
        StringBuffer text = new StringBuffer();
        CountClass c1 = new CountClass(text, 200);
        CountClass c2 = new CountClass(text, 100);
        System.out.println("main started");
        c1.run();
        c2.run();
        System.out.println("Result: \n" + text);
        System.out.println("main finished");
    }
}